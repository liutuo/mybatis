/**
 *    Copyright 2009-2019 the original author or authors.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package org.apache.ibatis.plugin;

import java.util.Properties;

/**
 *
 * 插件几乎是所有主流框架提供的一种扩展方式之一，插件可以用于记录日志，统计运行时性能，为核心功能提供额外的辅助支持。
 * 在mybatis中，插件是在内部是通过拦截器实现的。要开发自定义自定义插件，
 * 只要实现org.apache.ibatis.plugin.Interceptor接口即可，Interceptor接口定义如下：
 *
 * @author Clinton Begin
 */
public interface Interceptor {

  //执行代理类方法
  Object intercept(Invocation invocation) throws Throwable;

  // 用于创建代理对象
  default Object plugin(Object target) {
    return Plugin.wrap(target, this);
  }

  // 插件自定义属性
  default void setProperties(Properties properties) {
    // NOP
  }

}
